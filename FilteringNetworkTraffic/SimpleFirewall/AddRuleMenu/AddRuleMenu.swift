//
//  AddRuleMenu.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct AddRuleMenu: View {
	@State var name: String = ""
	@State var hostname: String = ""
	@State var port: String = ""
	@State var userId: String = ""
	@State private var selectedGroup = SimpleFirewallModel.RuleGroupsTypes.All
	@Binding var showSelf: Bool
	@EnvironmentObject var viewController: SimpleFirewallViewController
	
	var body: some View {
		ZStack {
			VStack(alignment: .leading, spacing: 0) {
				
				Group {
					Text("Add new Rule")
						.bold()
						.padding()
						.frame(maxWidth: .infinity, alignment: .center)

					Text("Enter name:")
						.padding()

					TextField("", text: $name)
						.padding(.horizontal)

					Text("Enter hostname or ip adress:")
						.padding()

					TextField("", text: $hostname)
						.padding(.horizontal)

					Text("Enter port:")
						.padding()

					
					TextField("", text: $port)
							.padding(.horizontal)
					
					
					Text("Enter userId:")
						.padding()
					
					TextField("", text: $userId)
						.padding(.horizontal)
					
					
					Picker("Choose a group:", selection: $selectedGroup) {
						Text("All").tag(SimpleFirewallModel.RuleGroupsTypes.All)
						Text("Default").tag(SimpleFirewallModel.RuleGroupsTypes.Default)
						Text("Preffered").tag(SimpleFirewallModel.RuleGroupsTypes.Preffered)
					}
					.padding(.top, 10)
					.padding()
					
				}
				
				Spacer()
				
				HStack {
					Button(action: { showSelf = false }, label: {
																Text("Cancel")
															})
															.padding()
				
					Spacer()
				
					Button(action: {
						showSelf = false
						viewController.addRule(FilterRule(id: IPCConnection.shared.rules.getUniqueId() ,
														  name: name,
														  hostname: hostname,
														  port: port,ruleGroup:
															selectedGroup.rawValue ,
														  filterAction: 2,
														  userId: userId))
					}, label: {
								Text("Add")
							  })
							  .padding()
				}
				
			}
		}
		.frame(maxWidth: 500, maxHeight: .infinity)
		.background(RoundedRectangle(cornerRadius: 10).stroke(Color.green, lineWidth: 2)
	)
	}
}
