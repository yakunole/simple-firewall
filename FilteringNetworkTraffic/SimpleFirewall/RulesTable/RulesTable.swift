//
//  RulesTable.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct RulesTable: View {
	typealias constants = RulesTableConstants
	@State var onHover = false
	@EnvironmentObject var viewController: SimpleFirewallViewController
		
		var body: some View {
			GeometryReader { geometry in
				VStack(spacing: 0) {
					
					Divider()
					
					RulesTableAttributes()
						.frame(height: RulesTableConstants.rulesTableHeight)
					
					Divider()
				
					if !viewController.shouldShowHistoryLogs() {
						createListOfRules()
					} else {
						createListOfLogs()
					}
					
				}
					Spacer()
			}
		}
	
	private func createListOfRules() -> some View {
		GeometryReader { geometry in
			List {
				let rules = viewController.getRulesToDisplay()
				ForEach(rules.indices, id: \.self) { index in
					let rule = rules[index]
					if index % 2 == 0 {
						RuleView(color: constants.evenRuleColor, rule: rule)
							.frame(height: geometry.size.height/constants.numberOfRules)
							.clipShape(RoundedRectangle(cornerRadius: constants.ruleCornerRadius))
					} else {
						RuleView(color: constants.oddRuleColor, rule: rule)
							.frame(height: geometry.size.height/constants.numberOfRules)
							.clipShape(RoundedRectangle(cornerRadius: constants.ruleCornerRadius))
					}
				}
			}
		}
	}
	
	
	private func createListOfLogs() -> some View {
		GeometryReader { geometry in
			List {
				let historyLogs = viewController.getLogsToDisplay()
				ForEach(historyLogs.indices, id: \.self) { index in
					if index % 2 == 0 {
						Log(text: historyLogs[index].message, color: constants.evenRuleColor)
							.frame(height: geometry.size.height/constants.numberOfRules)
							.clipShape(RoundedRectangle(cornerRadius: constants.ruleCornerRadius))
					} else {
						Log(text: historyLogs[index].message, color: constants.oddRuleColor)
							.frame(height: geometry.size.height/constants.numberOfRules)
							.clipShape(RoundedRectangle(cornerRadius: constants.ruleCornerRadius))
					}
				}
			}
		}
	}
	
	
}

struct RulesTableConstants {
	static let oddRuleColor = Color(red: 211/255, green: 211/255, blue: 211/255)
	static let evenRuleColor = Color.white
	static let ruleCornerRadius: CGFloat = 10
	static let numberOfRules: CGFloat = 16
	static let rulesTableHeight: CGFloat = 25
}

struct RulesTable_Previews: PreviewProvider {
	static var previews: some View {
		RulesTable()
	}
}
