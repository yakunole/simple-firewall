//
//  Log.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 26.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct Log: View {
	let text: String
	let color: Color
	@State var onHover = false
	
    var body: some View {
		HStack {
			Text(text)
				.padding()
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
		.foregroundColor(onHover ? Color.white : Color.black)
		.background(onHover ?  Color.green : color )
		.onHover(perform: { hovering in
			onHover = hovering
		})
    }
}

//struct HistoryLog_Previews: PreviewProvider {
//    static var previews: some View {
//        Log()
//    }
//}
