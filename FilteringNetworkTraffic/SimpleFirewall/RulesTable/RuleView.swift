//
//  RuleView.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct RuleView: View {
	
	@State var onHover = false
	@EnvironmentObject var viewController: SimpleFirewallViewController
	let color: Color
	let rule: FilterRule
	
	var body: some View {
		
		GeometryReader { geometry in
			HStack(spacing: 0) {
				
				let spacer = Spacer().frame(width: ruleViewConstants.spacerWidth)

				Group {
					spacer

					Text(rule.name)
						.frame(width: geometry.size.width/ruleViewConstants.sizeDivider,alignment: .leading)

					spacer

					Text(rule.hostname)
						.frame(width: geometry.size.width/ruleViewConstants.sizeDivider,alignment: .leading)

					spacer

					Text("\(rule.port)")
						.frame(width: geometry.size.width/ruleViewConstants.sizeDivider,alignment: .leading)

					spacer
				}


				if rule.filterAction == 1 {
					createAllowButton(geometry: geometry)
				} else {
					createBlockButton(geometry: geometry)
				}
					
				
				Spacer()
				
				createDeleteButton()
					
			}
			.frame(maxWidth: .infinity, maxHeight: .infinity)
			.foregroundColor(onHover ? Color.white : Color.black)
			.background(onHover ?  Color.green.opacity(ruleViewConstants.opacity) : color )
			.onHover(perform: { hovering in
				onHover = hovering
			})
		}
	}
	
	private func createBlockButton(geometry: GeometryProxy) -> some View {
		let horizontalPadding: CGFloat = 5
		let sizeDivider: CGFloat = 5
		
		return HStack {
			Circle()
				.fill(Color.red)
				.aspectRatio(contentMode: .fit)
				.padding(.horizontal, horizontalPadding)
				.frame(width: buttonConstants.frameWidth, height: buttonConstants.frameHeight)
			
			Text("Block")
				.onTapGesture {
					viewController.allowRule(rule.id)
				}
				.frame(width: geometry.size.width/sizeDivider,  alignment: .leading)
		}
	}
	
	private func createAllowButton(geometry: GeometryProxy) -> some View {
		let horizontalPadding: CGFloat = 5
		let sizeDivider: CGFloat = 5
		
		return HStack {
			Circle()
				.fill(Color.green)
				.aspectRatio(contentMode: .fit)
				.padding(.horizontal, horizontalPadding)
				.frame(width: buttonConstants.frameWidth, height: buttonConstants.frameHeight)
			
			Text("Allow")
				.onTapGesture {
					viewController.blockRule(rule.id)
				}
				.frame(width: geometry.size.width/sizeDivider,  alignment: .leading)
		}
	}
	
	private func createDeleteButton() -> some View {
		let backgroundColor = Color.gray
		let deleteButtonPadding: CGFloat = 8
		let frameWidth: CGFloat = 20
		let frameHeight: CGFloat = 20
		
		
		return Text("╳")
			.padding()
			.background(Circle()
							.fill(backgroundColor)
							.frame(width: frameWidth, height: frameHeight)
							.padding(deleteButtonPadding)
							.aspectRatio(contentMode: .fit))
			.onTapGesture {
				viewController.deleteRule(rule.id)
			}
	}
}

private struct ruleViewConstants {
	static let spacerWidth: CGFloat = 8
	static let deleteButtonColor = Color.gray
	static let sizeDivider: CGFloat = 5
	static let opacity: Double = 0.7
}

private struct buttonConstants {
	static let frameWidth: CGFloat = 20
	static let frameHeight: CGFloat = 20
}

//
//struct RuleView_Previews: PreviewProvider {
//    static var previews: some View {
//		RuleView(color: Color.red, rule: Model.Rule(id: 1, text: "All", hostname: "0.0.0.0", port: 433, action: 1))
//    }
//}
