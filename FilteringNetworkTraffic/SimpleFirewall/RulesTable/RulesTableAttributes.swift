//
//  RulesTableAttributes.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct RulesTableAttributes: View {
	@EnvironmentObject var viewController: SimpleFirewallViewController
	
	var body: some View {
		GeometryReader { geometry in
			HStack(spacing: 0) {
				let divider = Divider().padding(tableAttributesConstants.defaultPadding)
				
				ForEach(viewController.getRuleTableAttributes()) { attribute in
					
					divider
					
					Text(attribute.name)
						.frame(width: geometry.size.width/attribute.sizeDivider, alignment: .leading)
				}

				Spacer()
				}
		}
	}
}

private struct tableAttributesConstants {
	static let defaultPadding: CGFloat = 4
}

struct RulesTableAttributes_Previews: PreviewProvider {
	static var previews: some View {
		RulesTableAttributes()
	}
}
