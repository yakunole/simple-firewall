//
//  SimpleFirewall.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct SimpleFirewall: View {
	@State var showAddRuleMenu = false
	@ObservedObject var ViewController: SimpleFirewallViewController
	
    var body: some View {
		GeometryReader { geometry in
			ZStack {
				HStack(spacing: 0) {
					SidebarMenu()
						.frame(width: geometry.size.width/AppConstants.sidebarMenusizeDivider, height: geometry.size.height)
					
					VStack {
						TopBar(showAddRuleMenu: $showAddRuleMenu)
							.frame(height: geometry.size.height/AppConstants.topBarHeightDivider)
						
						RulesTable()
					}
					
				}
				.opacity(showAddRuleMenu ? AppConstants.opacityWhenAddRuleMuniIsNotShown : AppConstants.opacityWhenAddRuleMenuIsShown)
				.background(AppConstants.backgroundColor)
				
				if self.showAddRuleMenu {
					AddRuleMenu(showSelf: $showAddRuleMenu)
						.padding()
				}
				
			}
			.environmentObject(ViewController)
			.onAppear() {
				ViewController.viewWillAppear()
			}
			.onDisappear() {
				ViewController.viewWillDisappear()
			}
		}
    }
}

private struct AppConstants {
	static let opacityWhenAddRuleMenuIsShown: Double = 1
	static let opacityWhenAddRuleMuniIsNotShown: Double = 0
	static let backgroundColor = Color.white
	static let sidebarMenusizeDivider: CGFloat = 6
	static let topBarHeightDivider: CGFloat = 10
}
