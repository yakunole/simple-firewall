//
//  SidebarButton.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct SidebarButton: View {
	let name: String
	let pathToImage: String
	let indexNumber: Int
	@State private var onHover = false
	
	var body: some View {
		HStack {
			
			Image(pathToImage)
				.foregroundColor(onHover ? buttonConstants.onHoverForegroundColor : buttonConstants.onHoverBackgroundColor)
				.padding(.leading, buttonContentConstants.ImageLeadingPadding)
			
			Text(name)
			
			Spacer()
			
			Text("\(indexNumber)")
				.foregroundColor(.gray)
				.padding(buttonContentConstants.textInCirclePadding)
				.background(Circle().fill(buttonContentConstants.circleColor).padding(buttonContentConstants.circlePadding))
		}
		.foregroundColor(onHover ? buttonConstants.onHoverForegroundColor : buttonConstants.foregroundColor)
		.background(onHover ? buttonConstants.onHoverBackgroundColor : buttonConstants.backgroundColor)
		.clipShape(RoundedRectangle(cornerRadius: buttonConstants.cornerRadius))
		.padding(.trailing, buttonConstants.trailingPadding)
		.padding(.leading, buttonConstants.leadingPadding)
		.padding(.top, buttonConstants.topPadding)
		.onHover(perform: { hovering in
			onHover = hovering
		})
	}
}

private struct buttonConstants {
	static let trailingPadding: CGFloat = 10
	static let leadingPadding: CGFloat = 15
	static let topPadding: CGFloat = 3
	static let onHoverForegroundColor = Color.white
	static let onHoverBackgroundColor = Color.green
	static let foregroundColor = Color.black
	static let backgroundColor = Color(red: 237/255, green: 237/255, blue: 237/255)
	static let cornerRadius: CGFloat = 10
}

private struct buttonContentConstants {
	static let circleColor = Color.white
	static let ImageLeadingPadding: CGFloat = 30
	static let textInCirclePadding: CGFloat = 8
	static let circlePadding: CGFloat = 4
	static let nameLeadingPadding = 15
}
