//
//  SidebarMenu.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct SidebarMenu: View {
	@EnvironmentObject var viewController: SimpleFirewallViewController
	
	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			
			makeCategory(name: SidebarConstants.categories[0], pathToImage: SidebarConstants.folderImagePath)
		
			ForEach(viewController.getRuleGroups()) { ruleGroup in
			
				SidebarButton(name: ruleGroup.name, pathToImage: SidebarConstants.descriptionImagePath, indexNumber: ruleGroup.id)
					.onTapGesture {
						viewController.changeCurrentPage(groupId: ruleGroup.id, nameOfPage: ruleGroup.name, showLogs: false)
					}
			}

			makeCategory(name: SidebarConstants.categories[1], pathToImage: SidebarConstants.historyImagePath)
				.foregroundColor(.gray)
			
			ForEach(viewController.getHistoryLogsGroups()) { log in
				SidebarButton(name: log.name, pathToImage: SidebarConstants.historyImagePath, indexNumber: log.id)
					.onTapGesture {
						viewController.changeCurrentPage(groupId: log.id, nameOfPage: log.name, showLogs: true)
					}
			}
			
			makeCategory(name: SidebarConstants.categories[2], pathToImage: SidebarConstants.timelineImagePath)
				.foregroundColor(.gray)
			
			ForEach(viewController.getHistoryRulesGroups()) { ruleGroup in
				SidebarButton(name: ruleGroup.name, pathToImage: SidebarConstants.historyImagePath, indexNumber: 1)
					.onTapGesture {
						viewController.changeCurrentPage(groupId: ruleGroup.id, nameOfPage: ruleGroup.name, showLogs: false)
					}
			}
			
			Spacer()
		}
		.background(SidebarConstants.sidebarBackgroundColor)
	}
		
	private func makeCategory(name: String, pathToImage: String) -> some View {
		
		HStack {
			Image(pathToImage)
				.foregroundColor(SidebarConstants.imageForegroundColor)
				.padding(.top, SidebarConstants.imageTopPadding)
				.padding(.leading, SidebarConstants.imageLeadingPadding)
				.padding(.bottom, SidebarConstants.imageBottomPadding)
			
			Text(name)
				.padding(EdgeInsets(top: SidebarConstants.categoryTopPadding,
									leading: SidebarConstants.categoryLeadingPadding,
									bottom: SidebarConstants.categoryBottomPadding,
									trailing: SidebarConstants.categoryTrailingBottom))
		}
	}
	
}

private struct SidebarConstants {
	static let categories = ["Current","Logs","History"]
	static let historyImagePath = "outline_history_black_18pt"
	static let folderImagePath = "outline_folder_black_20pt"
	static let timelineImagePath = "outline_timeline_black_20pt"
	static let descriptionImagePath = "outline_description_black_18pt"
	static let categoryTopPadding: CGFloat = 20
	static let categoryLeadingPadding: CGFloat = 5
	static let categoryBottomPadding: CGFloat = 5
	static let categoryTrailingBottom: CGFloat = 0
	static let imageBottomPadding: CGFloat = 5
	static let imageTopPadding: CGFloat = 20
	static let imageLeadingPadding: CGFloat = 10
	static let sidebarBackgroundColor = Color(red: 237/255, green: 237/255, blue: 237/255)
	static let imageForegroundColor = Color.green
}

struct SidebarMenu_Previews: PreviewProvider {
	static var previews: some View {
		SidebarMenu()
	}
}
