//
//  SimpleFirewallModel.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation


struct SimpleFirewallModel {
	
	init(savedRules: [FilterRule]) {
		self.savedRules = savedRules
	}
	
	// MARK: PROPERTIES
	
	var filterStatus = FilterStatus.stopped
	private var logUniqeId = 0
	
	private(set) var currentGroupId = 1
	private(set) var currentPageName = "All"
	private(set) var showHistoryLogs = false
	
	private(set) var savedRules: [FilterRule]
	private(set) var savedLogs = [Log]()
	
	
	private(set) var rulesGroups = [RuleGroup(id: 1, name: "All"),
									RuleGroup(id: 2, name: "Default"),
									RuleGroup(id: 3, name: "Preffered")]
	
	private(set) var historyRulesGroups = [RuleGroup(id: 4, name: "All Rules")]

	
	private(set) var historyLogsGroups = [LogGroup(id: logsGroups.flowGroupId, name: "Flows"),
										  LogGroup(id: logsGroups.ruleGroupId, name: "Rules"),
										  LogGroup(id: logsGroups.actionGroupId, name: "Actions")]
	
	private(set) var rulesTableAttributes = [TableAttribute(id: 1,name: "Rule", sizeDivider: 5),
											 TableAttribute(id: 2,name: "Hostname", sizeDivider: 5),
											 TableAttribute(id: 3,name: "Port", sizeDivider: 5),
											 TableAttribute(id: 4,name: "Action", sizeDivider: 5)]
	
	
	// MARK: METHODS
		
	mutating func addRule(_ rule: FilterRule) {
		savedRules.append(rule)
	}

	mutating func deleteRule(_ ruleId: Int) {
		if let indexOfDisplayedRules = savedRules.firstIndex(where: { $0.id == ruleId }) {
			savedRules.remove(at: indexOfDisplayedRules)
		}
	}
	
	mutating func allowRule(_ ruleId: Int) {
		if let indexOfDisplayedRules = savedRules.firstIndex(where: { $0.id == ruleId }) {
			savedRules[indexOfDisplayedRules].filterAction = FilterRules.FilterActions.Allow.rawValue
		}
	}
	
	mutating func blockRule(_ ruleId: Int) {
		if let indexOfDisplayedRules = savedRules.firstIndex(where: { $0.id == ruleId }) {
			savedRules[indexOfDisplayedRules].filterAction = FilterRules.FilterActions.Block.rawValue
		}
	}

	mutating func changeCurrentPage(newPageName: String, newGroupId: Int) {
		currentPageName = newPageName
		currentGroupId = newGroupId
	}
	
	mutating func addLog(message: String, logGroup: Int) {
		let newLog = Log(id: logUniqeId, message: message, logGroup: logGroup)
		savedLogs.append(newLog)
		logUniqeId += 1
	}
	
	mutating func startShowingLogs() {
		showHistoryLogs = true
	}
	
	mutating func stopShowingLogs() {
		showHistoryLogs = false
	}
	
	
	struct RuleGroup: Identifiable {
		var id: Int
		let name: String
	}
		
	struct TableAttribute: Identifiable {
		var id: Int
		let name: String
		let sizeDivider: CGFloat
	}
	
	struct LogGroup: Identifiable {
		var id: Int
		let name: String
	}
	
	struct Log: Identifiable {
		var id: Int
		let message: String
		let logGroup: Int
	}

	enum RuleGroupsTypes: Int, CaseIterable {
		case All = 1
		case Default = 2
		case Preffered = 3
	}
	
	enum FilterStatus {
		case stopped
		case indeterminate
		case running
	}
	
}
