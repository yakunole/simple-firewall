/*
See LICENSE folder for this sample’s licensing information.

Abstract:
This file contains the implementation of the class that implements the NSApplicationDelegate protocol.
*/

import Cocoa
import SwiftUI


class AppDelegate: NSObject, NSApplicationDelegate {
	
	var window: NSWindow!

	func applicationDidFinishLaunching(_ aNotification: Notification) {
		// Create the SwiftUI view that provides the window contents.
		let viewController = SimpleFirewallViewController()
		let contentView = SimpleFirewall(ViewController: viewController)
			.frame(minWidth: 1250, maxWidth: .infinity, minHeight: 700, maxHeight: .infinity)

		// Create the window and set the content view.
		window = NSWindow(
			contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
			styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
			backing: .buffered, defer: false)
		window.isReleasedWhenClosed = false
		window.center()
		window.contentView = NSHostingView(rootView: contentView)
		window.makeKeyAndOrderFront(nil)
	}
}
