//
//  SimpleFirewallViewController.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation
import Cocoa
import NetworkExtension
import SystemExtensions
import os.log

class SimpleFirewallViewController: NSViewController, ObservableObject {
			
	// MARK: PROPERTIES
	
	@Published var model = SimpleFirewallModel(savedRules: IPCConnection.shared.rules.rules)
	var observer: Any?
	
	lazy var dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
		return formatter
	}()


	// Get the Bundle of the system extension.
	lazy var extensionBundle: Bundle = {

		let extensionsDirectoryURL = URL(fileURLWithPath: "Contents/Library/SystemExtensions", relativeTo: Bundle.main.bundleURL)
		let extensionURLs: [URL]
		do {
			extensionURLs = try FileManager.default.contentsOfDirectory(at: extensionsDirectoryURL,
																		includingPropertiesForKeys: nil,
																		options: .skipsHiddenFiles)
		} catch let error {
			fatalError("Failed to get the contents of \(extensionsDirectoryURL.absoluteString): \(error.localizedDescription)")
		}

		guard let extensionURL = extensionURLs.first else {
			fatalError("Failed to find any system extensions")
		}

		guard let extensionBundle = Bundle(url: extensionURL) else {
			fatalError("Failed to create a bundle with URL \(extensionURL.absoluteString)")
		}

		return extensionBundle
	}()
	
	
	// MARK: GETTERS
	
	func getFilterStatus() -> SimpleFirewallModel.FilterStatus {
		return model.filterStatus
	}
	
	func getRuleGroups() -> [SimpleFirewallModel.RuleGroup] {
		return model.rulesGroups
	}
	
	func getHistoryRulesGroups() -> [SimpleFirewallModel.RuleGroup] {
		return model.historyRulesGroups
	}
	
	func getRuleTableAttributes() -> [SimpleFirewallModel.TableAttribute] {
		return model.rulesTableAttributes
	}
	
	func getHistoryLogsGroups() -> [SimpleFirewallModel.LogGroup] {
		return model.historyLogsGroups
	}
	
	func shouldShowHistoryLogs() -> Bool {
		return model.showHistoryLogs
	}
	
	func getnameOfCurrentPage() -> String {
		return model.currentPageName
	}
	
	func getRulesToDisplay() -> [FilterRule] {
		if model.currentGroupId == 1 || model.currentGroupId == 4 {
			return model.savedRules
		}
		
		return model.savedRules.filter { $0.ruleGroup == model.currentGroupId }
	}
	
	func getLogsToDisplay() -> [SimpleFirewallModel.Log] {
		return model.savedLogs.filter { $0.logGroup == model.currentGroupId }
	}
		
	// MARK: UI EVENT HANDLERS
	
	func addRule(_ rule: FilterRule) {
		IPCConnection.shared.addNewRuleToProvider(rule)
		model.addRule(rule)
		createHistoryLog(message: "Added new rule with hostname: \(rule.hostname), port: \(rule.port)", logGroup: logsGroups.ruleGroupId)
	}

	func deleteRule(_ ruleId: Int) {
		let rule = model.savedRules.first(where: { $0.id == ruleId })
		createHistoryLog(message: "Deleted a rule with hostname: \(rule!.hostname), port: \(rule!.port)", logGroup: logsGroups.ruleGroupId)
		IPCConnection.shared.deleteRuleFromProvider(ruleId)
		model.deleteRule(ruleId)
	}

	func allowRule(_ ruleId: Int) {
		let rule = model.savedRules.first(where: { $0.id == ruleId })
		createHistoryLog(message: "Changed filter action for a rule with hostname: \(rule!.hostname), port: \(rule!.port) to Allow", logGroup: logsGroups.actionGroupId)
		IPCConnection.shared.allowRuleInProvider(ruleId)
		model.allowRule(ruleId)
	}

	func blockRule(_ ruleId: Int) {
		let rule = model.savedRules.first(where: { $0.id == ruleId })
		createHistoryLog(message: "Changed filter action for a rule with hostname: \(rule!.hostname), port: \(rule!.port) to Deny", logGroup: logsGroups.actionGroupId)
		IPCConnection.shared.blockRuleInProvider(ruleId)
		model.blockRule(ruleId)
	}
	
	func changeCurrentPage( groupId: Int, nameOfPage: String, showLogs: Bool) {
		if showLogs {
			model.startShowingLogs()
		} else {
			model.stopShowingLogs()
		}
		model.changeCurrentPage(newPageName: nameOfPage, newGroupId: groupId)
	}
	
	func changeFilterState() {
		if self.model.filterStatus == .running {
			stopFilter()
		} else if self.model.filterStatus == .stopped {
			startFilter()
		}
	}
	
	func startFilter() {
		self.model.filterStatus = .indeterminate
		guard !NEFilterManager.shared().isEnabled else {
			registerWithProvider()
			return
		}

		guard let extensionIdentifier = extensionBundle.bundleIdentifier else {
			self.model.filterStatus = .stopped
			return
		}

		// Start by activating the system extension
		let activationRequest = OSSystemExtensionRequest.activationRequest(forExtensionWithIdentifier: extensionIdentifier, queue: .main)
		activationRequest.delegate = self
		OSSystemExtensionManager.shared.submitRequest(activationRequest)
        DispatchQueue.main.asyncAfter(deadline: .now() + 300.0) { // Change `2.0` to the desired number of seconds.
                  // Code you want to be delayed
            self.stopFilter()}
	}

	func stopFilter() {

		let filterManager = NEFilterManager.shared()

		self.model.filterStatus = .indeterminate

		guard filterManager.isEnabled else {
			self.model.filterStatus = .stopped
			return
		}
		

		loadFilterConfiguration { success in
			guard success else {
				self.model.filterStatus = .running
				return
			}

			// Disable the content filter configuration
			filterManager.isEnabled = false
			filterManager.saveToPreferences { saveError in
				DispatchQueue.main.async {
					if let error = saveError {
						os_log("Failed to disable the filter configuration: %@", error.localizedDescription)
						self.model.filterStatus = .running
						return
					}

					self.model.filterStatus = .stopped
				}
			}
		}
	}
	
	// MARK: NSViewController

	override func viewWillAppear() {
		
		super.viewWillAppear()

		self.model.filterStatus = .indeterminate

		loadFilterConfiguration { success in
			guard success else {
				self.model.filterStatus = .stopped
				return
			}

			self.updateStatus()

			self.observer = NotificationCenter.default.addObserver(forName: .NEFilterConfigurationDidChange,
																   object: NEFilterManager.shared(),
																   queue: .main) { [weak self] _ in
				self?.updateStatus()
				
			}
		}
	}

	override func viewWillDisappear() {

		super.viewWillDisappear()

		guard let changeObserver = observer else {
			return
		}

		NotificationCenter.default.removeObserver(changeObserver, name: .NEFilterConfigurationDidChange, object: NEFilterManager.shared())
	}

	// MARK: Update the UI

	func updateStatus() {

		if NEFilterManager.shared().isEnabled {
			registerWithProvider()
		} else {
			self.model.filterStatus = .stopped
		}
	}



	// MARK: Content Filter Configuration Management

	func loadFilterConfiguration(completionHandler: @escaping (Bool) -> Void) {

		NEFilterManager.shared().loadFromPreferences { loadError in
			DispatchQueue.main.async {
				var success = true
				if let error = loadError {
					os_log("Failed to load the filter configuration: %@", error.localizedDescription)
					success = false
				}
				completionHandler(success)
			}
		}
	}

	func enableFilterConfiguration() {
		let filterManager = NEFilterManager.shared()

		guard !filterManager.isEnabled else {
			registerWithProvider()
			return
		}

		loadFilterConfiguration { success in

			guard success else {
				self.model.filterStatus = .stopped
				return
			}

			if filterManager.providerConfiguration == nil {
				let providerConfiguration = NEFilterProviderConfiguration()
				providerConfiguration.filterSockets = true
				providerConfiguration.filterPackets = false
				filterManager.providerConfiguration = providerConfiguration
				if let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
					filterManager.localizedDescription = appName
				}
			}

			filterManager.isEnabled = true

			filterManager.saveToPreferences { saveError in
				DispatchQueue.main.async {
					if let error = saveError {
						os_log("Failed to save the filter configuration: %@", error.localizedDescription)
						self.model.filterStatus = .stopped
						return
					}

					self.registerWithProvider()
				}
			}
		}
	}

	// MARK: ProviderCommunication

	func registerWithProvider() {
		IPCConnection.shared.register(withExtension: extensionBundle, delegate: self) { success in
			DispatchQueue.main.async {
				self.model.filterStatus = (success ? .running : .stopped)
			}
		}
	}
}

extension SimpleFirewallViewController: OSSystemExtensionRequestDelegate {

	// MARK: OSSystemExtensionActivationRequestDelegate

	func request(_ request: OSSystemExtensionRequest, didFinishWithResult result: OSSystemExtensionRequest.Result) {

		guard result == .completed else {
			os_log("Unexpected result %d for system extension request", result.rawValue)
			self.model.filterStatus = .stopped
			return
		}

		enableFilterConfiguration()
	}

	func request(_ request: OSSystemExtensionRequest, didFailWithError error: Error) {

		os_log("System extension request failed: %@", error.localizedDescription)
		self.model.filterStatus = .stopped
	}

	func requestNeedsUserApproval(_ request: OSSystemExtensionRequest) {

		os_log("Extension %@ requires user approval", request.identifier)
	}

	func request(_ request: OSSystemExtensionRequest,
				 actionForReplacingExtension existing: OSSystemExtensionProperties,
				 withExtension extension: OSSystemExtensionProperties) -> OSSystemExtensionRequest.ReplacementAction {

		os_log("Replacing extension %@ version %@ with version %@", request.identifier, existing.bundleShortVersion, `extension`.bundleShortVersion)
		return .replace
	}
}

extension SimpleFirewallViewController: AppCommunication {
	func createHistoryLog(message: String, logGroup: Int) {
		model.addLog(message: message, logGroup: logGroup)
	}
}



