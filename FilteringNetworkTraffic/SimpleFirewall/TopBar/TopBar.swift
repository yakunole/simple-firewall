//
//  TopBar.swift
//  SimpleFirewall
//
//  Created by Alex Yakunin on 23.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import SwiftUI

struct TopBar: View {
	@Binding var showAddRuleMenu: Bool
	@EnvironmentObject var viewController: SimpleFirewallViewController
	
	var body: some View {
		GeometryReader { geometry in
			HStack {
				
				RulesGroupIdentifier(nameOfGroup: viewController.getnameOfCurrentPage(), year: "2021")
				
				Spacer()
				
				isRunning()
				
				Button(action: { showAddRuleMenu = true }, label: {
					Image("outline_add_black_24pt")
				})
				.padding(.trailing, TopBarConstants.searchBarTrailingPadding)
			}
		}
	}
	
	private func isRunning() -> some View {
		if viewController.getFilterStatus() == .running {
			return createButton(text: "Stop", color: .green)
		} else if viewController.getFilterStatus() == .stopped {
			return createButton(text: "Start", color: .red)
		} else {
			return createButton(text: "Processing", color: .orange)
		}
	}
	
	
	private func createButton(text: String, color: Color) -> some View {
		HStack {
			Circle()
				.fill(color)
				.aspectRatio(contentMode: .fit)
				.frame(width: TopBarConstants.changeFilterStateButtonWidth, height: TopBarConstants.changeFilterStateButtonHeight)
			
			Button(action: { viewController.changeFilterState()  }, label: {
				Text(text)
			})
		}

	}
	
	struct RulesGroupIdentifier: View {
		let nameOfGroup: String
		let year: String
		
		var body: some View {
			VStack(alignment: .leading) {
				Text(nameOfGroup)
					.bold()
					.padding(EdgeInsets(top: TopBarConstants.nameOfGroupTopPadding, leading: TopBarConstants.nameOfGroupLeadingPadding, bottom: TopBarConstants.nameOfGroupBottomPadding, trailing: TopBarConstants.nameOfGroupTrailingPadding))
				
				Text(year)
					.foregroundColor(TopBarConstants.yearForegroundColor)
					.padding(.bottom, TopBarConstants.yearBottomPadding)
					.padding(.leading, TopBarConstants.yearLeadingPadding)
			}
		}
	}
}


private struct TopBarConstants {
	static let nameOfGroupTopPadding: CGFloat = 6
	static let nameOfGroupLeadingPadding: CGFloat = 15
	static let nameOfGroupBottomPadding: CGFloat = 0
	static let nameOfGroupTrailingPadding: CGFloat = 10
	static let yearBottomPadding: CGFloat = 6
	static let yearLeadingPadding: CGFloat = 15
	static let yearForegroundColor = Color.gray
	static let searchBarTrailingPadding: CGFloat = 10
	static let searchBarWidthDivider: CGFloat = 4
	static let changeFilterStateButtonWidth: CGFloat = 13
	static let changeFilterStateButtonHeight: CGFloat = 13
}


//struct TopBar_Previews: PreviewProvider {
//    static var previews: some View {
//        TopBar()
//    }
//}

