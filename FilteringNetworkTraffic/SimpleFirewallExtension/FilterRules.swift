//
//  FilterRules.swift
//  SimpleFirewallExtension
//
//  Created by Alex Yakunin on 25.07.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation
import os.log

class FilterRules: NSObject, Codable {
	
	enum Keys: String {
		case countUniqueId = "countUniqueId"
		case currentRules = "currentRules"
		case savedRules = "savedRules"
	}
	
	enum FilterActions: Int {
		case Allow = 1
		case Block = 2
	}
	
	func json() throws -> Data {
		return try JSONEncoder().encode(self)
	}
		
	init(url: URL) throws {
			let data = try Data(contentsOf: url)
			let arrayOfRules = try JSONDecoder().decode(FilterRules.self, from: data)
			self.currentRules = arrayOfRules.currentRules
			self.savedRules = arrayOfRules.savedRules
		}
	
	
	init(countUniqueId: Int, currentRules: [FilterRule], savedRules: [FilterRule]) {
		self.currentRules = currentRules
		self.savedRules = savedRules
	}
	

	override init() {
		
	}
	
	private var currentRules = [FilterRule]()
	private var savedRules = [FilterRule]()
	
	func getCurrentRules() -> [FilterRule] {
		return currentRules
	}
	
	func getSavedRules() -> [FilterRule] {
		return savedRules
	}
		
	// MARK: METHODS TO MODIFY SAVED RULES
	
	func addRule(_ rule: FilterRule) {
		currentRules.append(rule)
		savedRules.append(rule)
	}
	
	func deleteRule(_ ruleId: Int) {
		if let indexOfSeacrchedElement = currentRules.firstIndex(where: { $0.id == ruleId }) {
			currentRules.remove(at: indexOfSeacrchedElement)
		}
	}
	
	func blockRule(_ ruleId: Int) {
		if let indexOfSearchedElement = currentRules.firstIndex(where: { $0.id == ruleId }) {
			currentRules[indexOfSearchedElement].filterAction = FilterActions.Block.rawValue
		}
	}
	
	func allowRule(_ ruleId: Int) {
		if let indexOfSeacrchedElement = currentRules.firstIndex(where: { $0.id == ruleId }) {
			currentRules[indexOfSeacrchedElement].filterAction = FilterActions.Allow.rawValue
		}
	}
	
	func getRulesByGroupId(_ groupId: Int) -> [FilterRule] {
		if groupId == 1 {
			return currentRules
		} else {
			return currentRules.filter { $0.ruleGroup == groupId }
		}
	}
}

@objc(FilterRule) class FilterRule: NSObject, NSSecureCoding, Codable {
	
	static var supportsSecureCoding = true
	
	enum Keys: String {
		case id = "id"
		case name = "name"
		case hostname = "hostname"
		case port = "port"
		case ruleGroup = "ruleGroup"
		case filterAction = "filterAction"
		case userId = "userId"
	}
	
	func encode(with coder: NSCoder) {
		coder.encode(id, forKey: Keys.id.rawValue)
		coder.encode(name, forKey: Keys.name.rawValue)
		coder.encode(hostname, forKey: Keys.hostname.rawValue)
		coder.encode(port, forKey: Keys.port.rawValue)
		coder.encode(ruleGroup, forKey: Keys.ruleGroup.rawValue)
		coder.encode(filterAction, forKey: Keys.filterAction.rawValue)
		coder.encode(userId, forKey: Keys.userId.rawValue)
	}
	
	required convenience init?(coder: NSCoder) {
		
		let id = coder.decodeInteger(forKey: Keys.id.rawValue) as Int
		let name = coder.decodeObject(forKey: Keys.name.rawValue) as! String
		let hostname = coder.decodeObject(forKey: Keys.hostname.rawValue) as! String
		let port = coder.decodeObject(forKey: Keys.port.rawValue) as! String
		let ruleGroup = coder.decodeInteger(forKey: Keys.ruleGroup.rawValue) as Int
		let filterAction = coder.decodeInteger(forKey: Keys.filterAction.rawValue) as Int
		let userId = coder.decodeObject(forKey: Keys.userId.rawValue) as! String
		
		self.init(id: id, name: name, hostname: hostname, port: port, ruleGroup: ruleGroup, filterAction: filterAction, userId: userId)
	}

	var id: Int
	let name: String
	let hostname: String
	let port: String
	let ruleGroup: Int
	var filterAction: Int
	var userId: String

	init(id: Int, name: String, hostname: String, port: String, ruleGroup: Int, filterAction: Int, userId: String) {
		self.id = id
		self.name = name
		self.hostname = hostname
		self.port = port
		self.ruleGroup = ruleGroup
		self.filterAction = filterAction
		self.userId = userId
	}
}

struct Rules: Codable {
	var rules = [FilterRule]()
	var countUniqueId = 0
	
	mutating func getUniqueId() -> Int {
		let id = countUniqueId
		countUniqueId += 1
		return id
	}
	
	func json() throws -> Data {
		return try JSONEncoder().encode(self)
	}
	
	init(json: Data) throws  {
		self = try JSONDecoder().decode(Rules.self, from: json)
	}
	
	init(url: URL) throws {
		let data = try Data(contentsOf: url)
		self = try Rules(json: data)
	}
	
	init() {
		
	}
}
