//
//  object_methods.m
//  SimpleFirewall
//
//  Created by admin on 8/31/21.
//  Copyright © 2021 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "object_methods.h"

pid_t procID(NSData *sourceAppAuditToken){
    audit_token_t *token = (audit_token_t*)sourceAppAuditToken.bytes;
    pid_t pid = audit_token_to_pid(*token);
    return pid;
}

uid_t getUserId(NSData *sourceAppAuditToken){
    audit_token_t *token = (audit_token_t*)sourceAppAuditToken.bytes;
    uid_t ruid = audit_token_to_ruid(*token);
    //os_log(OS_LOG_DEFAULT, "Some message\n Username: %{public}s", username);
    return ruid;
}

char* getUsername(NSData *sourceAppAuditToken){
    struct passwd *uid_pw;
    uid_pw = getpwuid(getUserId(sourceAppAuditToken));
    char* username = uid_pw->pw_name;
    return username;
}
