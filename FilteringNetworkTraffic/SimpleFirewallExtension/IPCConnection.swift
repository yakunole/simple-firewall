/*
See LICENSE folder for this sample’s licensing information.

Abstract:
This file contains the implementation of the app <-> provider IPC connection
*/

import Foundation
import os.log
import Network

/// App --> Provider IPC
 @objc protocol ProviderCommunication {
	func getCurrentRules() -> [FilterRule]
	func getSavedRules() -> [FilterRule]
	func addNewFilterRule(_ encodedRules: Data)
	func deleteFilterRule(_ ruleId: Int)
	func blockFilterRule(_ ruleId: Int)
	func allowFilterRule(_ ruleId: Int)
	func getRulesByGroupId(_ groupId: Int) -> [FilterRule]
	func getUniqueId() -> Int
    func register(_ completionHandler: @escaping (Bool) -> Void)
}

/// Provider --> App IPC
@objc protocol AppCommunication {
	func createHistoryLog(message: String, logGroup: Int)
}

enum FlowInfoKey: String {
    case localPort
    case remoteAddress
}

struct logsGroups {
	static let flowGroupId = 1
	static let ruleGroupId = 2
	static let actionGroupId = 3
}

/// The IPCConnection class is used by both the app and the system extension to communicate with each other
class IPCConnection: NSObject {

	// Load previously saved rules
	override init() {
		if let url = Autosave.url, let autosavedRules = try? Rules(url: url) {
			rules = autosavedRules
		} else {
			rules = Rules()
		}
		
		SavedRules = FilterRules()
	}

	
    // MARK: Properties
	
	var rules: Rules {
		didSet {
			autosave()
		}
	}
	private(set) var SavedRules: FilterRules
	var listener: NSXPCListener? 
    var currentConnection: NSXPCConnection?
	weak var delegate: AppCommunication?
    static let shared = IPCConnection()

    // MARK: Methods

	// struct for saving informetion about rules given by a user
	private struct Autosave {
		static let filename = "Rules.json"
		static var url: URL? {
			let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
			return documentDirectory?.appendingPathComponent(filename)
		}
	}

	// function called when array with rules is modified
	 func autosave() {
		if let url = Autosave.url {
			save(to: url)
		}
	}

	private func save(to url: URL) {
		let thisFunction = "\(String(describing: self)).\(#function)"
		do {
			let data: Data = try rules.json()
			try data.write(to: url)
			print("\(thisFunction) - sucess")
		} catch let encodingError where encodingError is EncodingError {
			print("\(thisFunction) - encodingError")
		} catch {
			print("\(thisFunction) - \(error)")
		}
	}

	
    /**
        The NetworkExtension framework registers a Mach service with the name in the system extension's NEMachServiceName Info.plist key.
        The Mach service name must be prefixed with one of the app groups in the system extension's com.apple.security.application-groups entitlement.
        Any process in the same app group can use the Mach service to communicate with the system extension.
     */
    private func extensionMachServiceName(from bundle: Bundle) -> String {
		
        guard let networkExtensionKeys = bundle.object(forInfoDictionaryKey: "NetworkExtension") as? [String: Any],
            let machServiceName = networkExtensionKeys["NEMachServiceName"] as? String else {
                fatalError("Mach service name is missing from the Info.plist")
        }

        return machServiceName
    }

    func startListener() {
        let machServiceName = extensionMachServiceName(from: Bundle.main)
        os_log("Starting XPC listener for mach service %@", machServiceName)

        let newListener = NSXPCListener(machServiceName: machServiceName)
        newListener.delegate = self
        newListener.resume()
        listener = newListener
    }
	
	/// This method is called by the provider to add history log to UI
	func createHistoryLog(message: String, logGroup: Int) {
		guard let connection = currentConnection else {
			os_log("Cannot prompt user because the app isn't registered")
			return
		}

		guard let appProxy = connection.remoteObjectProxyWithErrorHandler({ promptError in
			os_log("Failed to prompt the user: %@", promptError.localizedDescription)
			self.currentConnection = nil
		}) as? AppCommunication else {
			fatalError("Failed to create a remote object proxy for the app")
		}

		appProxy.createHistoryLog(message: message, logGroup: logGroup)
	}
	

    /// This method is called by the app to register with the provider running in the system extension.
    func register(withExtension bundle: Bundle, delegate: AppCommunication, completionHandler: @escaping (Bool) -> Void) {
        self.delegate = delegate
        guard currentConnection == nil else {
            os_log("Already registered with the provider")
            completionHandler(true)
            return
        }

        let machServiceName = extensionMachServiceName(from: bundle)
        let newConnection = NSXPCConnection(machServiceName: machServiceName, options: [])

        // The exported object is the delegate.
        newConnection.exportedInterface = NSXPCInterface(with: AppCommunication.self)
        newConnection.exportedObject = delegate

        // The remote object is the provider's IPCConnection instance.
        newConnection.remoteObjectInterface = NSXPCInterface(with: ProviderCommunication.self)

        currentConnection = newConnection
        newConnection.resume()

        guard let providerProxy = newConnection.remoteObjectProxyWithErrorHandler({ registerError in
            os_log("Failed to register with the provider: %@", registerError.localizedDescription)
            self.currentConnection?.invalidate()
            self.currentConnection = nil
            completionHandler(false)
        }) as? ProviderCommunication else {
            fatalError("Failed to create a remote object proxy for the provider")
        }

		os_log("My log registered")
        providerProxy.register(completionHandler)
	}
	
	func addNewRuleToProvider(_ rule: FilterRule) {
		guard let connection = currentConnection else {
			os_log("My log Not registered with the provider")
			return
		}

		guard let providerProxy = connection.remoteObjectProxyWithErrorHandler({ registerError in
			os_log("My log Here Failed to add rule with error %@", registerError.localizedDescription)
		}) as? ProviderCommunication else {
			fatalError("Failed to create connection with app")
		}

		if let data = try? NSKeyedArchiver.archivedData(withRootObject: rule, requiringSecureCoding: false) {
			os_log("My log, data encoded")
			let decodedRule = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! FilterRule
			os_log("My log, data decoded with hostname %@ and port %@", decodedRule.hostname, decodedRule.port)
			
			rules.rules.append(rule)
			providerProxy.addNewFilterRule(data)
		}
	}
	
	func deleteRuleFromProvider(_ ruleId: Int) {
		guard let connection = currentConnection else {
			os_log("My log Not registered with the provider")
			return
		}

		guard let providerProxy = connection.remoteObjectProxyWithErrorHandler({ registerError in
			os_log("My log Here Failed to add rule with error %@", registerError.localizedDescription)
		}) as? ProviderCommunication else {
			fatalError("Failed to create connection with app")
		}
		
		if let ruleIndex = rules.rules.firstIndex(where: { $0.id == ruleId }) {
			rules.rules.remove(at: ruleIndex)
		}
		providerProxy.deleteFilterRule(ruleId)
	}
	
	func allowRuleInProvider(_ ruleId: Int) {
		guard let connection = currentConnection else {
			os_log("My log Not registered with the provider")
			return
		}

		guard let providerProxy = connection.remoteObjectProxyWithErrorHandler({ registerError in
			os_log("My log Here Failed to add rule with error %@", registerError.localizedDescription)
		}) as? ProviderCommunication else {
			fatalError("Failed to create connection with app")
		}
		
		if let ruleIndex = rules.rules.firstIndex(where: { $0.id == ruleId }) {
			rules.rules[ruleIndex].filterAction = 1
		}
		providerProxy.allowFilterRule(ruleId)
	}
	
	func blockRuleInProvider(_ ruleId: Int) {
		guard let connection = currentConnection else {
			os_log("My log Not registered with the provider")
			return
		}

		guard let providerProxy = connection.remoteObjectProxyWithErrorHandler({ registerError in
			os_log("My log Here Failed to add rule with error %@", registerError.localizedDescription)
		}) as? ProviderCommunication else {
			fatalError("Failed to create connection with app")
		}
		
		if let ruleIndex = rules.rules.firstIndex(where: { $0.id == ruleId }) {
			rules.rules[ruleIndex].filterAction = 2
		}
		providerProxy.blockFilterRule(ruleId)
	}
}

extension IPCConnection: NSXPCListenerDelegate {

    // MARK: NSXPCListenerDelegate

    func listener(_ listener: NSXPCListener, shouldAcceptNewConnection newConnection: NSXPCConnection) -> Bool {

        // The exported object is this IPCConnection instance.
        newConnection.exportedInterface = NSXPCInterface(with: ProviderCommunication.self)
        newConnection.exportedObject = self

        // The remote object is the delegate of the app's IPCConnection instance.
        newConnection.remoteObjectInterface = NSXPCInterface(with: AppCommunication.self)

        newConnection.invalidationHandler = {
            self.currentConnection = nil
        }

        newConnection.interruptionHandler = {
            self.currentConnection = nil
        }

        currentConnection = newConnection
        newConnection.resume()

        return true
    }
}

extension IPCConnection: ProviderCommunication {
	
	func getUniqueId() -> Int {
		return 1
	}
	
	// MARK: ProviderCommunication
		
	func getCurrentRules() -> [FilterRule] {
		SavedRules.getCurrentRules()
	}
	
	func getSavedRules() -> [FilterRule] {
		SavedRules.getSavedRules()
	}
	
	func addNewFilterRule(_ encodedRule: Data) {
		let rule: FilterRule = try! NSKeyedUnarchiver.unarchivedObject(ofClass: FilterRule.self, from: encodedRule)!
		SavedRules.addRule(rule)
	}
		
	func deleteFilterRule(_ ruleId: Int) {
		SavedRules.deleteRule(ruleId)
	}
	
	func allowFilterRule(_ ruleId: Int) {
		SavedRules.allowRule(ruleId)
	}
	
	func blockFilterRule(_ ruleId: Int) {
		SavedRules.blockRule(ruleId)
	}
	
	func getRulesByGroupId(_ groupId: Int) -> [FilterRule] {
		SavedRules.getRulesByGroupId(groupId)
	}
	
    func register(_ completionHandler: @escaping (Bool) -> Void) {
        os_log("My log App registered")
        completionHandler(true)
    }
}


