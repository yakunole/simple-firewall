//
//  object_methods.h
//  SimpleFirewall
//
//  Created by admin on 8/31/21.
//  Copyright © 2021 Apple. All rights reserved.
//

#ifndef object_methods_h
#define object_methods_h
#import <Foundation/Foundation.h>
#include <EndpointSecurity/EndpointSecurity.h>
#include <dispatch/queue.h>
#include <bsm/libbsm.h>
#include <stdio.h>
#include <os/log.h>

#import <NetworkExtension/NEFilterFlow.h>
#include <sys/xattr.h>
#import <NetworkExtension/NEFilterProvider.h>
#include <pwd.h>


//int proc2ID(NEFilterFlow *flow);
pid_t procID(NSData *sourceAppAuditToken);
uid_t getUserId(NSData *sourceAppAuditToken);
char* getUsername(NSData *sourceAppAuditToken);

#endif /* object_methods_h */
