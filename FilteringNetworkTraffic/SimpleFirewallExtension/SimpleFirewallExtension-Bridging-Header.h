//
//  SimpleFirewallExtension-Bridging-Header.h
//  SimpleFirewall
//
//  Created by admin on 8/31/21.
//  Copyright © 2021 Apple. All rights reserved.
//

#ifndef SimpleFirewallExtension_Bridging_Header_h
#define SimpleFirewallExtension_Bridging_Header_h
#import "object_methods.h"

//int procID(audit_token_t df);

pid_t procID(NSData *sourceAppAuditToken);
uid_t getUserId(NSData *sourceAppAuditToken);
char* getUsername(NSData *sourceAppAuditToken);

#endif /* SimpleFirewallExtension_Bridging_Header_h */
