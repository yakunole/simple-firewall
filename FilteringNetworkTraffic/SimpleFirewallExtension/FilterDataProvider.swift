/*
See LICENSE folder for this sample’s licensing information.

Abstract:
This file contains the implementation of the NEFilterDataProvider sub-class.
*/

import NetworkExtension
import os.log
 

func verifyTarget(ipOrUrl: String) -> Int {
	if ipOrUrl.isValidIpAddress {
		os_log("My log is a valid ip adress %s", ipOrUrl); return 1
	} else if ipOrUrl.isValidHostname {
		os_log("My log is a valid url %s", ipOrUrl); return 2
	} else {
		print("My log is not valid %s", ipOrUrl); return 0
	}
}
enum Regex {
	static let ipAddress = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
	static let hostname = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$"
}

extension String {
	var isValidIpAddress: Bool {
		return self.matches(pattern: Regex.ipAddress)
	}
	
	var isValidHostname: Bool {
		return self.matches(pattern: Regex.hostname)
	}
	
	private func matches(pattern: String) -> Bool {
		return self.range(of: pattern,
						  options: .regularExpression,
						  range: nil,
						  locale: nil) != nil
	}
}


/**
    The FilterDataProvider class handles connections that match the installed rules by prompting
    the user to allow or deny the connections.
 */
class FilterDataProvider: NEFilterDataProvider {

    // MARK: NEFilterDataProvider
	
    override func startFilter(completionHandler: @escaping (Error?) -> Void) {


		// here we decide to make all decision about flows in handleNewFlow function)
		let filterSettings = NEFilterSettings(rules: [], defaultAction: .filterData)

		// apply rules
		apply(filterSettings) { error in
			if let applyError = error {
				os_log("Failed to apply filter settings: %@", applyError.localizedDescription)
			}
			completionHandler(error)
		}
	}
		
    override func stopFilter(with reason: NEProviderStopReason, completionHandler: @escaping () -> Void) {

        completionHandler()
    }
	
    func checkIfRuleExists(address: String, port: String, user: String) -> NEFilterNewFlowVerdict {
				
		if let appliedRule = IPCConnection.shared.getCurrentRules().first(where: { ($0.hostname == address || "www."+$0.hostname == address)
			&& ($0.port == port || $0.port == "0" ) && ($0.userId == user || $0.userId == "0")
			}) {

			if appliedRule.filterAction != NEFilterAction.allow.rawValue {
				os_log("My log Blocked flow with with hostname %@ and port %@ in array and action %d", appliedRule.hostname, appliedRule.port
					   , appliedRule.filterAction)
				IPCConnection.shared.createHistoryLog(message: "Blocked flow with with hostname \(appliedRule.hostname) and port \(appliedRule.port) in array and action \(appliedRule.filterAction)", logGroup: logsGroups.flowGroupId)
				return .drop()
			}
		}
		
		os_log("My log allowed rule with with hostname %@ and port %@ in array and action %d", address, port)
		IPCConnection.shared.createHistoryLog(message: "Allowed flow with with hostname \(address) and port \(port))", logGroup: logsGroups.flowGroupId)
		return .allow()
	}
    
    func getUsername(uid: uid_t)-> String{
            let p = getpwuid(uid)!
            return (String(cString: p.pointee.pw_name))
        }
    
	override func handleNewFlow(_ flow: NEFilterFlow) -> NEFilterNewFlowVerdict {

		let token = (flow.sourceAppAuditToken)
		let uid = getUserId(token)
		let user = getUsername(uid: uid)
		
		guard let url = flow.url?.host else {
				return .allow()
		}
			
		let input = verifyTarget(ipOrUrl: url)
			
		if input == 1 {
			guard let socketFlow:NEFilterSocketFlow = flow as? NEFilterSocketFlow,
				  let hostEndpoint:NWHostEndpoint = socketFlow.remoteEndpoint as? NWHostEndpoint else
			{
					return .allow()
			}
				
			return checkIfRuleExists(address: hostEndpoint.hostname, port: hostEndpoint.port, user: user)
		} else if input == 2 {
				os_log("My log func called with url %@", flow.url!.host!)
				return checkIfRuleExists(address: url, port: "0", user: user)
			} else {
				return .allow()
			}
    }
}

struct FilterRulesConstants {
	static let defaultPrefix = 0
	static let localNetwork: NWHostEndpoint? = nil
	static let trafficDirection = NETrafficDirection.inbound
}
