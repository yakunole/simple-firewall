//
//  SavingFunctionalityTests.swift
//  SavingFunctionalityTests
//
//  Created by Alex Yakunin on 08.08.2021.
//  Copyright © 2021 Apple. All rights reserved.
//

import XCTest
@testable import SimpleFirewall

class SavingFunctionalityTests: XCTestCase {
	
	func test_valid_decoding() throws {
		let url = URL(fileURLWithPath: "/Users/alexyakunin/Desktop/macos_ne_cource_project/FilteringNetworkTraffic/SavingFunctionalityTests/SavedRules.json")
		XCTAssertNoThrow(try FilterRules(url: url))
	}
	
	func test_invalid_decoding() throws {
		
		let url = URL(fileURLWithPath: "/InvalidRules.json")
		XCTAssertThrowsError(try FilterRules(url: url))
	}
	
	func test_invalid_url() throws {
		let url = URL(fileURLWithPath: "No such file")
		XCTAssertThrowsError(try FilterRules(url: url))
	}
	
	func test_encoding() throws {
		var test = FilterRules()
	
		test.addRule(FilterRules.FilterRule(hostname: "0.0.0.0", port: "112", filterAction: 1))
		test.addRule(FilterRules.FilterRule(hostname: "123", port: "80", filterAction: 2))
		
		XCTAssertNoThrow(try test.json())
	}
	
	func test_stop_of_rule() {
		var test = FilterRules()
		
		test.addRule(FilterRules.FilterRule(hostname: "1.1.1.1", port: "20", filterAction: 2))
		test.stopRule(0)
	
		XCTAssert(test.getCurrentRules()[0].filterAction == 1)
	}
	
	func test_stop_of_rule_if_id_is_invalid() {
		var test = FilterRules()
		
		test.stopRule(12)
	}
	
}
