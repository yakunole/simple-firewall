#!/bin/bash
 
readonly CODE_SIGN_IDENTITY=85466A3B48CB94B8ED60825FC3B1A954DC1DAA38
readonly CODE_SIGN_TEAM_ID=53P2ZT8BF2
 
set -e
set -x
 
security  unlock-keychain -p '{}' /Users/${USER}/Library/Keychains/login.keychain
codesign --sign $CODE_SIGN_IDENTITY \
    --entitlements SimpleFirewallExtension/SimpleFirewallExtension.entitlements \
    --options runtime --verbose --force --deep \
    <path_to_app>/SimpleFirewall.app/Contents/Library/SystemExtensions/com.example.apple-samplecode.SimpleFirewall$CODE_SIGN_TEAM_ID.SimpleFirewallExtension.systemextension
 
codesign --verify --verbose \
   <path_to_app>/SimpleFirewall.app/Contents/Library/SystemExtensions/com.example.apple-samplecode.SimpleFirewall$CODE_SIGN_TEAM_ID.SimpleFirewallExtension.systemextension
 
codesign --sign $CODE_SIGN_IDENTITY \
    --entitlements SimpleFirewall/SimpleFirewall.entitlements\
    --options runtime --verbose --force \
    <path_to_app>/SimpleFirewall.app
 
codesign \
    --verify --verbose \
    <path_to_app>/SimpleFirewall.app
